# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table diseno (
  id_diseno                 bigint auto_increment not null,
  fecha_subida              datetime(6),
  formato                   varchar(255),
  nombre                    varchar(255),
  apellidos                 varchar(255),
  email                     varchar(255),
  precio                    double,
  id_estado                 bigint not null,
  id_proyecto               bigint not null,
  constraint pk_diseno primary key (id_diseno))
;

create table entidad (
  id_entidad                bigint auto_increment not null,
  email                     varchar(255),
  nombre                    varchar(255),
  apellido                  varchar(255),
  password                  varchar(255),
  id_tipo                   bigint not null,
  constraint pk_entidad primary key (id_entidad))
;

create table entidad_rol (
  id_entidad_rol            bigint auto_increment not null,
  id_entidad                bigint not null,
  id_rol                    bigint not null,
  constraint pk_entidad_rol primary key (id_entidad_rol))
;

create table parametro (
  id_parametro              bigint auto_increment not null,
  titulo                    varchar(255),
  descripcion               varchar(255),
  id_tipo                   bigint not null,
  constraint pk_parametro primary key (id_parametro))
;

create table parametro_sistema (
  id_parametro_sistema      bigint auto_increment not null,
  titulo                    varchar(255),
  descripcion               varchar(255),
  valor                     varchar(255),
  constraint pk_parametro_sistema primary key (id_parametro_sistema))
;

create table proyecto (
  id_proyecto               bigint auto_increment not null,
  fecha_publicacion         datetime(6),
  titulo                    varchar(255),
  descripcion               varchar(255),
  valor                     double,
  id_entidad                bigint not null,
  id_estado                 bigint not null,
  constraint pk_proyecto primary key (id_proyecto))
;

create table rol (
  id_rol                    bigint auto_increment not null,
  nombre                    varchar(255),
  descripcion               varchar(255),
  constraint pk_rol primary key (id_rol))
;

create table tipo (
  id_tipo                   bigint auto_increment not null,
  titulo                    varchar(255),
  descripcion               varchar(255),
  constraint pk_tipo primary key (id_tipo))
;

create table web (
  id_web                    bigint auto_increment not null,
  titulo                    varchar(255),
  descripcion               varchar(255),
  url                       varchar(255),
  id_entidad                bigint not null,
  constraint pk_web primary key (id_web))
;

alter table diseno add constraint fk_diseno_estado_1 foreign key (id_estado) references parametro (id_parametro) on delete restrict on update restrict;
create index ix_diseno_estado_1 on diseno (id_estado);
alter table diseno add constraint fk_diseno_proyecto_2 foreign key (id_proyecto) references proyecto (id_proyecto) on delete restrict on update restrict;
create index ix_diseno_proyecto_2 on diseno (id_proyecto);
alter table entidad add constraint fk_entidad_tipo_3 foreign key (id_tipo) references parametro (id_parametro) on delete restrict on update restrict;
create index ix_entidad_tipo_3 on entidad (id_tipo);
alter table entidad_rol add constraint fk_entidad_rol_entidad_4 foreign key (id_entidad) references entidad (id_entidad) on delete restrict on update restrict;
create index ix_entidad_rol_entidad_4 on entidad_rol (id_entidad);
alter table entidad_rol add constraint fk_entidad_rol_rol_5 foreign key (id_rol) references rol (id_rol) on delete restrict on update restrict;
create index ix_entidad_rol_rol_5 on entidad_rol (id_rol);
alter table parametro add constraint fk_parametro_tipo_6 foreign key (id_tipo) references tipo (id_tipo) on delete restrict on update restrict;
create index ix_parametro_tipo_6 on parametro (id_tipo);
alter table proyecto add constraint fk_proyecto_entidad_7 foreign key (id_entidad) references entidad (id_entidad) on delete restrict on update restrict;
create index ix_proyecto_entidad_7 on proyecto (id_entidad);
alter table proyecto add constraint fk_proyecto_estado_8 foreign key (id_estado) references parametro (id_parametro) on delete restrict on update restrict;
create index ix_proyecto_estado_8 on proyecto (id_estado);
alter table web add constraint fk_web_entidad_9 foreign key (id_entidad) references entidad (id_entidad) on delete restrict on update restrict;
create index ix_web_entidad_9 on web (id_entidad);



# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table diseno;

drop table entidad;

drop table entidad_rol;

drop table parametro;

drop table parametro_sistema;

drop table proyecto;

drop table rol;

drop table tipo;

drop table web;

SET FOREIGN_KEY_CHECKS=1;

