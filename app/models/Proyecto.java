package models;

import com.avaje.ebean.Model;
import play.data.format.*;
import play.data.validation.*;
import sun.rmi.runtime.Log;

import javax.management.Query;
import javax.persistence.*;
import javax.persistence.EntityManager;
import java.io.Console;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Proyecto extends Model {

	private static final long serialVersionUID = 1516533259537001927L;

	@Id
	@Column(name = "id_proyecto")
	public Long id;

	@Column(name="fecha_publicacion")
	private Date fechaPublicacion;

	private String titulo;

	private String descripcion;

	private Double valor;

	@Column(name="id_entidad",nullable = false)
	private Long idEntidad;

	@ManyToOne
	@JoinColumn(name = "id_entidad",insertable=false,updatable=false)
	public Entidad entidad;
	public Entidad getEntidad() {
		return Entidad.find.where().idEq(idEntidad).findUnique();
	}

	@Column(name="id_estado",nullable = false)
	private Long idEstado;

	@ManyToOne
	@JoinColumn(name = "id_estado",insertable=false,updatable=false)
	public Parametro estado;
	public Parametro getParametro() {
		return Parametro.find.where().idEq(idEstado).findUnique();
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "proyecto")
	private List<Diseno> disenos;

	public List<Diseno> getDisenosImagen(){
		return disenos;
	}
	public List<Diseno> getDisenos(){
		return Diseno.find.orderBy("id desc").where().ilike("idProyecto", id+"").findList();
	}
	public List<Diseno> getDisenosDisponibles(){
		return Diseno.find.orderBy("id desc").where().ilike("idProyecto", id+"").ilike("idEstado", 2+"").findList();
	}

	public void setDisenos(List<Diseno> disenos) {
		this.disenos = disenos;
	}

	public Proyecto() {

	}

	public Long getIdEntidad() {
		return idEntidad;
	}

	public void setIdEntidad(Long idEntidad) {
		this.idEntidad = idEntidad;
	}

	public void setEntidad(Entidad entidad) {
		this.entidad = entidad;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFechaPublicacion() {
		return fechaPublicacion;
	}
	public String getFechaPublicacionFormateada() {
		SimpleDateFormat format = new SimpleDateFormat("d-M-yyyy hh:mm:ss");
		return format.format(fechaPublicacion);
	}

	public void setFechaPublicacion(Date fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Long getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(Long idEstado) {
		this.idEstado = idEstado;
	}

	public Parametro getEstado() {
		return estado;
	}

	public void setEstado(Parametro estado) {
		this.estado = estado;
	}

	public static Finder<Long, Proyecto> find = new Finder<Long, Proyecto>(Proyecto.class);

	public static List<Proyecto> proyectosPorEntidad(Long idEntidad, Long pagina){
		List<Proyecto> proyectos =  Proyecto.find.orderBy("fechaPublicacion desc")
			.where()
			.ilike("idEntidad", idEntidad.toString())
			.findPagedList((pagina.intValue()-1),10)
			.getList();
		return proyectos;
	}

	public static Long contarProyectosPorEntidad(Long idEntidad, Long pagina){
		return (long)Proyecto.find.orderBy("fechaPublicacion desc")
				.where()
				.ilike("idEntidad", idEntidad.toString())
				.findPagedList((pagina.intValue()-1),10)
				.getTotalRowCount();
	}

	public Long contarDisenos(Long idProyecto){
		return (long)Diseno.find.orderBy("fechaPublicacion desc")
				.where()
				.ilike("idProyecto", id.toString())
				.findRowCount();
	}



	public static List<Proyecto> proyectosPorUrl(String url){
		List<Proyecto> lstProyectos = new ArrayList<Proyecto>();
		Web web = Web.find.where().ilike("url", url).findUnique();
		try {
			if (web.getId() != null) {
				lstProyectos.addAll(proyectosPorEntidad(web.getIdEntidad(),1L));
			}
		}catch (NullPointerException e){
			System.out.println("no existe pagina");
		}

		return lstProyectos;
	}
}