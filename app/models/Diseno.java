package models;

import javax.persistence.*;

import com.avaje.ebean.Model;
import play.data.format.*;
import play.data.validation.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Diseno extends Model {

	private static final long serialVersionUID = 1516533259537001927L;

	@Id
	@Column(name = "id_diseno")
	private Long id;

	@Column(name="fecha_subida")
	private Date fechaSubida;

	private String formato;

	private String nombre;

	private String apellidos;

	private String email;

	private Double precio;

	@Transient
	private String encode;

	@Column(name="id_estado",nullable = false)
	private Long idEstado;

	@ManyToOne
	@JoinColumn(name = "id_estado",insertable=false,updatable=false)
	public Parametro estado;
	public Parametro getParametro() {
		return Parametro.find.where().idEq(id).findUnique();
	}


	@Column(name="id_proyecto",nullable = false)
	private Long idProyecto;

	@ManyToOne
	@JoinColumn(name = "id_proyecto",insertable=false,updatable=false)
	public Proyecto proyecto;
	public Proyecto getProyecto() {
		return Proyecto.find.where().idEq(id).findUnique();
	}

	public static Finder<Long, Diseno> find = new Finder<Long, Diseno>(Diseno.class);

    public Diseno() {

    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFechaSubida() {
		return fechaSubida;
	}

	public void setFechaSubida(Date fechaSubida) {
		this.fechaSubida = fechaSubida;
	}

	public String getFechaSubidaFormateada() {
		SimpleDateFormat format = new SimpleDateFormat("d-M-yyyy hh:mm:ss");
		return format.format(fechaSubida);
	}

	public String getFormato() {
		return formato;
	}

	public void setFormato(String formato) {
		this.formato = formato;
	}

	public Long getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(Long idEstado) {
		this.idEstado = idEstado;
	}

	public Parametro getEstado() {
		return estado;
	}

	public void setEstado(Parametro estado) {
		this.estado = estado;
	}

	public Long getIdProyecto() {
		return idProyecto;
	}

	public void setIdProyecto(Long idProyecto) {
		this.idProyecto = idProyecto;
	}

	public void setProyecto(Proyecto proyecto) {
		this.proyecto = proyecto;
	}

	public String getEncode() {
		return encode;
	}

	public void setEncode(String encode) {
		this.encode = encode;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public static List<Diseno> disenosPorProyecto(Long idProyecto, Long pagina){
		List<Diseno> Disenos =  Diseno.find.orderBy("fechaSubida desc")
				.where()
				.ilike("idProyecto", idProyecto.toString())
				.findPagedList((pagina.intValue()-1),10)
				.getList();
		return Disenos;
	}

	public static Long contarDisenosPorProyecto(Long idProyecto, Long pagina){
		return (long)Diseno.find.orderBy("fechaSubida desc")
			.where()
			.ilike("idProyecto", idProyecto.toString())
			.findPagedList((pagina.intValue()-1),10)
			.getTotalRowCount();
	}

}