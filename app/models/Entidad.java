package models;

import javax.persistence.*;

import com.avaje.ebean.Model;
import play.data.format.*;
import play.data.validation.*;

import java.util.ArrayList;
import java.util.List;


@Entity
public class Entidad extends Model {


	private static final long serialVersionUID = 1516533259537001927L;

	@Id
	@Column(name = "id_entidad")
	private Long id;

	private String email;

    private String nombre;

    private String apellido;

    private String password;

    @Column(name="id_tipo",nullable=false)
    private Long idTipo;

    @ManyToOne
    @JoinColumn(name = "id_tipo",insertable=false,updatable=false)
    private Parametro tipo;
    public Parametro getTipo() {
        return Parametro.find.where().idEq(getId()).findUnique();
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "entidad")
    private List<Web> webs;
    public List<Web> getWebs(){
        List<Web> lstWebs = Web.find.where().ilike("idEntidad", id+"").findList();
        return lstWebs;
    }

    public String obtenerUrl(){
        return ((Web)Web.find.where().ilike("idEntidad", id+"").findUnique()).getUrl();
    }

    public static Finder<Long, Entidad> find = new Finder<Long,Entidad>(Entidad.class);

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(Long idTipo) {
        this.idTipo = idTipo;
    }

    public static Entidad porEmail(String email){
        Entidad entidad = Entidad.find.where().ilike("email", email).findUnique();
        return Entidad.find.where().ilike("email", email).findUnique();
    }

}