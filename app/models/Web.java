package models;

import com.avaje.ebean.Model;
import play.data.format.*;
import play.data.validation.*;

import javax.persistence.*;

@Entity
public class Web extends Model {


	private static final long serialVersionUID = 1516533259537001927L;

	@Id
	@Column(name = "id_web")
	private Long id;

	private String titulo;

	private String descripcion;

	private String url;

	@Column(name="id_entidad",nullable = false)
	private Long idEntidad;

	@ManyToOne
	@JoinColumn(name = "id_entidad",insertable=false,updatable=false)
	public Entidad entidad;
	public Entidad getEntidad() {
		return Entidad.find.where().idEq(idEntidad).findUnique();
	}
	public static Finder<Long, Web> find = new Finder<Long, Web>(Web.class);

    public Web() {

    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getIdEntidad() {
		return idEntidad;
	}

	public void setIdEntidad(Long idEntidad) {
		this.idEntidad = idEntidad;
	}
}