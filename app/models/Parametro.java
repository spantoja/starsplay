package models;

import com.avaje.ebean.Model;
import play.data.format.*;
import play.data.validation.*;

import javax.persistence.*;

@Entity
public class Parametro extends Model {


	private static final long serialVersionUID = 1516533259537001927L;

	@Id
	@Column(name = "id_parametro")
	private Long id;

	private String titulo;

	private String descripcion;

	@Column(name="id_tipo",nullable=false)
	private Long idTipo;

	@ManyToOne
	@JoinColumn(name = "id_tipo",insertable=false,updatable=false)
	public Tipo tipo;
	public Tipo getTipo() {

		return Tipo.find.where().idEq(id).findUnique();
	}

	public static Finder<Long, Parametro> find = new Finder<Long, Parametro>(Parametro.class);

    public Parametro() {
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Long getIdTipo() {
		return idTipo;
	}

	public void setIdTipo(Long idTipo) {
		this.idTipo = idTipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}
}