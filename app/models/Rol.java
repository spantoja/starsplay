package models;

import com.avaje.ebean.Model;
import play.data.format.*;
import play.data.validation.*;

import javax.persistence.*;

@Entity
public class Rol extends Model {


	private static final long serialVersionUID = 1516533259537001927L;

	@Id
	@Column(name = "id_rol")
	private Long id;

	private String nombre;

	private String descripcion;

    public static Finder<Long, Rol> find = new Finder<Long, Rol>(Rol.class);

    public Rol() {
    }
}