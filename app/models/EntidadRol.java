package models;

import com.avaje.ebean.Model;
import play.data.format.*;
import play.data.validation.*;

import javax.persistence.*;


@Entity
public class EntidadRol extends Model {


	private static final long serialVersionUID = 1516533259537001927L;

	@Id
	@Column(name = "id_entidad_rol")
	private Long id;

    @Column(name="id_entidad",nullable = false)
    private Long idEntidad;

    @ManyToOne
    @JoinColumn(name = "id_entidad",insertable=false,updatable=false)
    public Entidad entidad;
    public Entidad getEntidad() {

        return Entidad.find.where().idEq(id).findUnique();
    }

    @Column(name="id_rol",nullable = false)
    private Long idRol;

    @ManyToOne
    @JoinColumn(name = "id_rol",insertable=false,updatable=false)
    public Rol rol;
    public Rol getRol() {

        return Rol.find.where().idEq(id).findUnique();
    }


    public static Finder<Long, EntidadRol> find = new Finder<Long, EntidadRol>(EntidadRol.class);

}