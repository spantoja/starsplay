package models;

import com.avaje.ebean.Model;
import play.data.format.*;
import play.data.validation.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class ParametroSistema extends Model {


	private static final long serialVersionUID = 1516533259537001927L;

	@Id
	@Column(name = "id_parametro_sistema")
	private Long id;

	private String titulo;

    private String descripcion;

    private String valor;

	public static Finder<Long, ParametroSistema> find = new Finder<Long, ParametroSistema>(ParametroSistema.class);

    public ParametroSistema() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}