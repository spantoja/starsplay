package models;

import java.io.Serializable;

/**
 * Created by ServioAndres on 19/02/2016.
 * DTO para el envio de información de la páginación
 */
public class PaginacionDTO implements Serializable{

    private Long registros;
    private Long paginas;
    private Long paginaActual;

    public Long getRegistros() {
        return registros;
    }

    public void setRegistros(Long registros) {
        this.registros = registros;
    }

    public Long getPaginas() {
        return paginas;
    }

    public void setPaginas(Long paginas) {
        this.paginas = paginas;
    }

    public Long getPaginaActual() {
        return paginaActual;
    }

    public void setPaginaActual(Long paginaActual) {
        this.paginaActual = paginaActual;
    }
}
