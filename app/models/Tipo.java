package models;

import com.avaje.ebean.Model;
import play.data.format.*;
import play.data.validation.*;

import javax.persistence.*;

@Entity
public class Tipo extends Model {


	private static final long serialVersionUID = 1516533259537001927L;

	@Id
	@Column(name = "id_tipo")
	private Long id;

	private String titulo;

	private String descripcion;

    public static Finder<Long, Tipo> find = new Finder<Long,Tipo>(Tipo.class);

    public Tipo() {
    }
}