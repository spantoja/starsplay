package controllers;

import com.avaje.ebean.Model;
import play.*;
import play.mvc.*;
import play.data.*;
import scala.util.parsing.json.JSONObject;
import views.html.*;
import models.*;

public class LoginController extends Controller  {

    public Result verForm() {
        Form<LoginDTO> formLogin = Form.form(LoginDTO.class);
        return ok(views.html.publico.login.render(formLogin));
    }

    public Result salir(){
        session().clear();
        return ok(views.html.publico.index.render("Esto es el index"));
    }

    public Result validarAcceso() {

        Form<LoginDTO> formData = Form.form(LoginDTO.class).bindFromRequest();
        System.out.println("Obteniendo entidad con correo: "+formData.get().getCorreo());
        Entidad entidad = Entidad.porEmail(formData.get().getCorreo());

        if (formData.hasErrors()) {
            return badRequest(views.html.publico.login.render(formData));
        } else {
            if(entidad==null){
                System.out.println("No existe la entidad con correo: "+formData.get().getCorreo());
                flash("error","El usuario o contraseña ingresados es incorrecto");
                return badRequest(views.html.publico.login.render(formData));
            }else if(entidad.getPassword().equals(formData.get().getContrasena())){
                System.out.println("Se encontro la entidad con correo: "+formData.get().getCorreo());
                session().clear();
                session("entidad", entidad.getNombre());
                session("idEntidad", entidad.getId().toString());
                session("url", entidad.getId().toString() + entidad.getNombre().replace(" ",""));
                return redirect(routes.HomePrivadoController.index());
            }else{
                System.out.println("No existe la entidad con correo: "+formData.get().getCorreo());
                flash("error","El usuario o contraseña ingresados es incorrecto");
                return badRequest(views.html.publico.login.render(formData));
            }

        }

    }

}
