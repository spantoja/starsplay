package controllers;

import play.*;
import play.mvc.*;
import views.html.privado.*;

public class HomePrivadoController extends Controller {

    public Result index() {
        if(session("entidad")==null)return redirect("/index");
        return ok(index.render());
    }

}
