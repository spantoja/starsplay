package controllers;

import models.LoginDTO;
import models.RegistroDTO;
import models.Entidad;
import models.Web;
import play.*;
import play.mvc.*;
import play.data.*;

import views.html.publico.*;


public class RegistroController extends Controller {

    public Result index() {
        return ok(index.render("Esto es el index"));
    }

    public Result verForm() {
        Form<RegistroDTO> formRegistro = Form.form(RegistroDTO.class);
        return ok(registro.render(formRegistro));
    }

    public Result registrar() {
        System.out.println("Invoca registrar: ");
        Form<RegistroDTO> formData = Form.form(RegistroDTO.class).bindFromRequest();
        System.out.println("Datos recibidos formulario: "+ formData);
        if (formData.hasErrors()) {
            flash("error", "Por favor verifique la información del formulario.");
            System.out.println("Datos Errroneos");
            return badRequest(registro.render(formData));
        }else{
            System.out.println("Datos Recibidos : " + formData.get().getNombres() + formData.get().isAceptaTerminos());
            if(!formData.get().getContrasena().equals(formData.get().getConfirmaContrasena())){
                flash("error", "La contraseñas ingresadas son diferentes.");
                return badRequest(registro.render(formData));
            }
            if(formData.get().getContrasena().trim().length()<6){
                flash("error", "La contraseña debe tener al menos 6 caracteres");
                return badRequest(registro.render(formData));
            }
            Entidad entidad = new Entidad();
            entidad.setNombre(formData.get().getEmpresa());
            entidad.setPassword(formData.get().getContrasena());
            entidad.setEmail(formData.get().getCorreo());
            entidad.setIdTipo(4L);
            entidad.save();
            Web web = new Web();
            web.setUrl(entidad.getId().toString() + entidad.getNombre().replace(" ",""));
            web.setIdEntidad(entidad.getId());
            web.save();
        }
        flash("success", "Registro Creado Exitosamente");
        Form<LoginDTO> formLogin = Form.form(LoginDTO.class);
        return ok(views.html.publico.login.render(formLogin));
    }
}
