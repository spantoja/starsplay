package controllers;

import play.*;
import play.mvc.*;
import views.html.publico.*;

public class HomeController extends Controller {

    public Result index() {

        return ok(index.render("Esto es el index"));
    }


    public Result verDesarrolladores() {

        return ok(desarrolladores.render());
    }

}
